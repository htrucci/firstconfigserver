package com.htrucci.firstconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class FirstconfigserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstconfigserverApplication.class, args);
	}
}
